<?php

return [
    'config_dir'  => 'novum.brp',
    'namespace'   => 'NovumBrp',
    'protocol'    => isset($_SERVER['IS_DEVEL']) ? 'http' : 'https',
    'live_domain' => 'gemeente.demo.novum.nu',
    'test_domain' => 'gemeente.test.demo.novum.nu',
    'dev_domain'  => 'gemeente.demo.novum.nuidev.nl',

];
